const mongoose = require("mongoose");

const config = require("./../../configs/app.config");
const logger = require("./../../services/logger.service");

let db = null;
function connectDB(name) {
  mongoose.set("useNewUrlParser", true);
  mongoose.set("useFindAndModify", false);
  mongoose.set("useCreateIndex", true);
  mongoose.set(
    "useUnifiedTopology",
    !["development"].includes(config.env) // true for production
  );
  const uri = config.mongo.host.uri;
  mongoose.connect(uri, {
    readPreference: "secondaryPreferred",
    keepAlive: true,
    serverSelectionTimeoutMS: 5000,
    connectTimeoutMS: 5000,
  });

  db = mongoose.connection;

  db.on("connected", function () {
    logger.info(`MongoDB ${name} connected`);
  });

  db.on("disconnected", function () {
    logger.warn(`MongoDB ${name} disconnected`);
  });

  db.on("reconnected", function () {
    logger.info(`MongoDB ${name} reconnected`);
  });

  db.on("error", function (err) {
    logger.error(`Error connection MongoDB ${name}`);
    console.error(err);
    // If first connect fails because mongod is down, try again later.
    // This is only needed for first connect, not for runtime reconnects.
    // See: https://github.com/Automattic/mongoose/issues/5169
    // Wait for a bit, then try to connect again

    // [PNC]: removed the if clause to retry on every connection error
    setTimeout(function () {
      logger.info(`Retrying first connect for MongoDB ${name}...`);
      db.openUri(uri).catch(() => {});
      // Why the empty catch?
      // Well, errors thrown by db.open() will also be passed to .on('error'),
      // so we can handle them there, no need to log anything in the catch here.
      // But we still need this empty catch to avoid unhandled rejections.
    }, 5 * 1000);
  });

  db.on("reconnectFailed", function () {
    logger.info(`MongoDB ${name} reconnectFailed`);
  });
}

if (db === null) {
  connectDB("chola");
}

module.exports = db;
