/* istanbul ignore file */
"use strict";

const tracer = require("cls-rtracer");
const { createLogger, format, transports } = require("winston");
const config = require("../configs/app.config");

const rTracerFormat = format.printf(
  ({ level, message, timestamp, ...meta }) => {
    const rid = tracer.id();
    if (rid)
      return `${timestamp} :: ${level} :: [${tracer.id()}] :: ${message} ${
        meta ? JSON.stringify(meta) : ""
      }`;
    else
      return `${timestamp} :: ${level} :: ${message} ${
        meta ? JSON.stringify(meta) : ""
      }`;
  }
);
const options = {
  level: config.logging.level || "debug",
  handleExceptions: true,
  json: true,
  colorize: true,
};

const logger = createLogger({
  level: config.logging.level || "debug",
  defaultMeta: { service: "chola" },
  format: format.combine(
    format.timestamp({
      format: "YYYY-MM-DD HH:mm:ss",
    }),
    format.errors({ stack: true }),
    format.colorize(),
    format.splat(),
    format.simple(),
    rTracerFormat
  ),
});

// logger.stream = {
//   write(message, encoding) {
//     // use the 'info' log level so the output will be picked up by both transports (file and console)
//     console.info(message);
//   },
// };

logger.add(
  new transports.Console({
    transports: [new transports.Console(options)],
  })
);

module.exports = logger;
