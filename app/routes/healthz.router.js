'use strict';

const express = require('express');

const healthzRouter = express.Router();

const { checkHealth } = require('../common/health-check');
/**
 * @swagger
 *
 * /_healthz:
 *   get:
 *    description: Get Healthz
 *    summary: Get Healthz
 *    operationId: getHealthz
 *    responses:
 *      '200':
 *        description: Success
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                ok:
 *                  type: string
 *      '4XX':
 *          description: Error
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: "#/components/schemas/FileAttachmentErrorResponse"
 */
healthzRouter.get('/_healthz', async (req, res) => {
    // res.json({
    //     "ok": "ok"
    // });
    const result = await checkHealth();
    if (result.ok) {
        return res.json({ ok: 'ok' });
    } else {
        return res.json({ ok: 'no', message: result.message });
    }
});

module.exports = healthzRouter;