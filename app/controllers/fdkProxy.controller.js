const logger = require("../services/logger.service");
const constants = require("../common/constants");
const { STATUS_CODES } = constants;

async function getPlatformApplications(req, res, next) {
  try {
    let {
      platformClient,
      query: { search, limit, page },
    } = req;
    limit = limit || 200;
    limit = parseInt(limit);
    let mongoQuery = {
      is_active: true,
      ...(search && { name: { $regex: `.*${search}.*`, $options: "i" } }),
    };
    console.log(encodeURIComponent(JSON.stringify(mongoQuery)));
    let platformApps = await platformClient.configuration.getApplications({
      pageSize: 200,
      pageNo: 1,
      q: JSON.stringify(mongoQuery),
    });
    //fake pagination ignored in above to maintain order same as platform
    if (platformApps.items && limit) {
      platformApps.items = platformApps.items.slice(
        (page - 1) * limit,
        page * limit + 1
      );
    }
    platformApps.page.current = page;
    platformApps.page.size = limit;
    platformApps.page.has_next = platformApps.items.length > limit;
    if (platformApps.items.length > limit) {
      platformApps.items.pop();
    }
    return res.status(STATUS_CODES.SUCCESS).json({
      statusCode: STATUS_CODES.SUCCESS,
      success: true,
      applications: platformApps.items,
      page: platformApps.page,
    });
  } catch (error) {
    logger.error("Error in retrieving platform applications", error);
    next(error);
  }
}

async function getPlatformCollections(req, res, next) {
  try {
    const {
      platformClient,
      query: { is_active, search, page, limit, application_id },
    } = req;

    let searchQuery = {
      ...(search && { q: `${search}` }),
      isActive: is_active || true,
      pageNo: page || 1,
      pageSize: limit || 10,
      type: "items",
    };

    const platformCollections = await platformClient
      .application(application_id)
      .catalog.getAllCollections(searchQuery);

    return res.status(STATUS_CODES.SUCCESS).json({
      statusCode: STATUS_CODES.SUCCESS,
      success: true,
      collections: platformCollections.items,
      page: platformCollections.page,
    });
  } catch (error) {
    logger.error("Error in retrieving platform collections", error);
    next(error);
  }
}

async function getPlatformCollectionTags(req, res, next) {
  try {
    const { platformClient } = req;

    const tags = await platformClient.catalog.getProductTags();

    return res.status(STATUS_CODES.SUCCESS).json({
      statusCode: STATUS_CODES.SUCCESS,
      success: true,
      tags,
    });
  } catch (error) {
    logger.error("Error in retrieving platform collections", error);
    next(error);
  }
}

async function getPlatformSpecificCollection(req, res, next) {
  try {
    const {
      platformClient,
      query: { slug, application_id },
    } = req;

    const platformCollection = await platformClient
      .application(application_id)
      .catalog.getCollectionDetail({ slug: slug });

    return res.status(STATUS_CODES.SUCCESS).json({
      statusCode: STATUS_CODES.SUCCESS,
      success: true,
      collections: platformCollection,
    });
  } catch (error) {
    logger.error("Error in retrieving platform collections", error);
    next(error);
  }
}

async function createPlatformCollection(req, res) {
  try {
    const {
      platformClient,
      query: { application_id },
      body: payload,
    } = req;
    const slug = payload.slug;
    let mpayload = {
      name: payload.name,
      logo: payload.logo,
      description: payload.description,
      tags: payload.tags,
      banners: payload.banners,
      slug: slug,
      type: payload.type,
      app_id: application_id,
    };
    // await platformClient.application(application_id).catalog.updateCollection({ id: slug, body: mpayload });
    const response = await platformClient
      .application(application_id)
      .catalog.createCollection({ body: mpayload });
    return res.status(200).json(response);
  } catch (error) {
    logger.error("Fail to create collection", error);
    response.error = error.details.errors;
    return res.status(500).json({
      success: false,
      message: "Failed to Update",
    });
  }
}

async function updatePlatformCollection(req, res) {
  try {
    const {
      platformClient,
      query: { application_id },
      body: payload,
    } = req;
    // const slug = payload.slug;
    const uid = payload.uid;
    let mpayload = {
      name: payload.name,
      logo: payload.logo,
      description: payload.description,
      tags: payload.tags,
      banners: payload.banners,
    };
    const resp = await platformClient
      .application(application_id)
      .catalog.updateCollection({ id: uid, body: mpayload });

    return res.status(200).json(resp);
    // await platformClient.application(application_id).catalog.createCollection({ body: mpayload });
  } catch (error) {
    logger.error("Error in updating collections", error);
    return res.status(500).json({
      success: false,
      message: error.details.errors,
    });
  }
}

module.exports = {
  getPlatformApplications,
  getPlatformCollections,
  getPlatformCollectionTags,
  getPlatformSpecificCollection,
  createPlatformCollection,
  updatePlatformCollection,
};
