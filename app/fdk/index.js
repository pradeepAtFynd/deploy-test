/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */

const { setupFdk } = require("fdk-extension-javascript/express");
const { RedisStorage } = require("fdk-extension-javascript/express/storage");

const config = require("../configs/app.config");
const { appRedis } = require("../connections/redis/index.js");
const getFdk = async () => {
  return setupFdk(
    {
      api_key: config.extension.api_key,
      api_secret: config.extension.api_secret,
      base_url: config.extension.base_url,
      scopes: ["company/product"],
      callbacks: {
        setup: async function (data) {
          console.log("SETUP::", data);
        },
        auth: async function (req) {
          return `${req.extension.base_url}/company/${req.query["company_id"]}`;
        },

        install: async function (data) {
          console.log("INSTALL::", data);
        },

        uninstall: async function (data) {
          console.log("UNINSTALL::", data);
          // Write your code here to cleanup data related to extension
          // If task is time taking then process it async on other process.
        },
      },
      storage: new RedisStorage(appRedis, "chola_integration"),
      access_mode: "offline",
      cluster: config.cluster_url,
    },
    true
  );
};

module.exports = getFdk;
