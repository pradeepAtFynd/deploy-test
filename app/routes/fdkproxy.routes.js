'use strict';

const { getPlatformApplications,getPlatformCollections,getPlatformCollectionTags,getPlatformSpecificCollection,createPlatformCollection,updatePlatformCollection} =require('../controllers/fdkProxy.controller');

const express = require('express');

const fdkProxyRouter = express.Router();

fdkProxyRouter.get("/applications", getPlatformApplications)
fdkProxyRouter.get("/collections", getPlatformCollections)
fdkProxyRouter.get("/tags", getPlatformCollectionTags)
fdkProxyRouter.get("/collection", getPlatformSpecificCollection);
fdkProxyRouter.post("/collection", createPlatformCollection);
fdkProxyRouter.put("/collection", updatePlatformCollection);

// fdkProxyRouter.get("/products", getPlatformProducts)



module.exports = fdkProxyRouter;