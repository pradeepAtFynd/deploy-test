const config = require("./app/configs/app.config");
const logger = require("./app/services/logger.service");
const main = require("./app/servers/main");
require("./app/connections/index");
require("dotenv").config();

const port = config.port || 5080;
(async () => {
  logger.info("SERVER_INITIALIZED");
  const app = await main.init();

  app.listen(port, () => {
    console.log("port:", port);
    logger.info(`Example app listening at ${port}`);
  });
})();
