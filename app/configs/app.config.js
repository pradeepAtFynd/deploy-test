/* eslint-disable camelcase */
/* istanbul ignore file */
"use strict";

const convict = require("convict");
const mongodbUri = require("mongodb-uri");

convict.addFormat({
  name: "mongo-uri",
  validate: function (val) {
    const parsed = mongodbUri.parse(val);
    mongodbUri.format(parsed);
  },
  coerce: function (urlString) {
    if (urlString) {
      const parsed = mongodbUri.parse(urlString);
      urlString = mongodbUri.format(parsed);
    }
    return urlString;
  },
});
let config = convict({
  env: {
    doc: "The application environment.",
    format: ["production", "development", "test"],
    default: "development",
    env: "NODE_ENV",
  },
  environment: {
    doc: "env",
    format: String,
    default: "fyndx1",
    env: "ENV",
    arg: "env",
  },
  platform: {
    doc: "",
    format: String,
    default: "FYND",
    env: "PLATFORM",
    arg: "platform",
  },
  extension: {
    api_key: {
      doc: "extension api key",
      default: "63245fdc2815c33aa38e550c",
      env: "EXTENSION_API_KEY",
    },
    api_secret: {
      doc: "extension api secret",
      default: "lLpi-tfkL2Sug-3",
      env: "EXTENSION_API_SECRET",
    },
    base_url: {
      doc: "extension base_url",
      default: "https://4cff-117-193-213-111.in.ngrok.io",
      env: "EXTENSION_BASE_URL",
    },
    vue_base_url: {
      doc: "extension vue_base_url",
      default: "https://8e77-14-142-183-234.in.ngrok.io",
      env: "VUE_APP_EXTENSION_BASE_URL",
    },
  },
  browser_config: {
    host_main_url: {
      doc: "Host Main URL",
      format: String,
      default: "https://8e77-14-142-183-234.in.ngrok.io",
      env: "EXTENSION_BASE_URL",
    },
  },
  port: {
    doc: "The port to bind",
    format: "port",
    default: 5080,
    env: "PORT",
    arg: "port",
  },
  logging: {
    level: "debug",
  },
  cluster_url: {
    doc: "Fynd Platform Domain",
    format: String,
    default: "https://api.fyndx1.de",
    env: "EXTENSION_CLUSTER_URL",
    arg: "extension_cluster_url",
  },
  redis: {
    chola: {
      doc: "Redis URL of host.",
      format: String,
      default: "redis://red-cfpeuc82i3mo4bqen9k0:6379",
      // default:
      //   "rediss://red-cfpeuc82i3mo4bqen9k0:t2yToKzbEM2JYLZuUKVx8cxWothrrLtc@singapore-redis.render.com:6379",
      env: "REDIS_CHOLA_READ_WRITE",
      arg: "redis_chola_read_only",
    },
  },
  mongo: {
    host: {
      uri: {
        doc: "host mongo",
        format: "mongo-uri",
        default: "mongodb://localhost:27017/chola_desk",
        env: "MONGO_CHOLA_DESK_READ_WRITE",
        arg: "mongo_chola_desk_read_write",
      },
    },
  },
  chola_desk_domain: {
    doc: "chola domain",
    format: String,
    default: "test",
    env: "CHOLA_DESK_DOMAIN",
    arg: "chola_desk_domain",
  },
});

// Perform validation
config.validate({
  allowed: "strict",
});
config = config.get();

module.exports = config;
