const Redis = require("ioredis");

const config = require("../../configs/app.config");

const logger = require("../../services/logger.service");

const REDIS_HOST = process.env.REDIS_URL || config.redis.chola;
logger.info(`Redis url :${REDIS_HOST}.`);

function connect(name, uri) {
  try {
    const db = new Redis(uri, {
      reconnectOnError: function (err) {
        var targetError = "EAI_AGAIN";
        if (err.message.includes(targetError)) {
          return true;
        }
      },
    });
    db.on("connect", () => {
      logger.info(`Redis ${name} connected.`);
    });
    db.on("ready", () => {
      logger.info(`Redis ${name} is ready`);
    });
    db.on("error", (err) => {
      logger.error(`Redis ${name} got error`, err);
    });
    db.on("close", () => {
      logger.warn(`Redis ${name} is closed`);
    });
    db.on("reconnecting", () => {
      logger.error(`Redis ${name} got reconnecting`);
    });
    return db;
  } catch (err) {
    logger.error("caught error in redis connect function", err, {
      name,
      uri,
    });
  }
}

const appRedis = connect("Host Read Write", REDIS_HOST);

module.exports = { appRedis };
