const logger = require("../services/logger.service");

module.exports = function (req, res, next) {
  const { method, hostname, path, body, query, params } = req;
  logger.info("API Request", { method, hostname, path, body, query, params });

  // res.on("finish", () => {})
  next();
};
