const config = require("../configs/app.config");
const logger = require("../services/logger.service");

module.exports = function (err, req, res, next) {
  err = err || {};
  let statusCode = 500;

  if (["ValidationError", "CastError"].includes(err.name)) {
    statusCode = 400;
  }

  if (err.statusCode) {
    statusCode = err.statusCode;
  }

  let errorBody = {
    message: err.errors || err.message || err,
    code: err.code,
    sentry: res.sentry,
  };

  if (["fyndx0"].includes(config.environment)) {
    errorBody["stack"] = err.stack;
    logger.debug(err);
  }
  res.status(statusCode).json(errorBody);
};
