const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const express = require("express");
const path = require("path");
const packageJson = require("../../../package.json");
const getFdk = require("./fdk");
const fdkProxyRouter = require("../../routes/fdkproxy.routes");
const { requestLogger } = require("../../middlewares");

async function init() {
  const app = express();

  const fdkExtension = await getFdk();

  app.use(cookieParser("ext.session"));
  app.use(bodyParser.json({ limit: "10mb" }));
  app.use(express.static(path.join(__dirname, "./../../../", "build")));

  /**
   * *******************
   * HEALTH BLOCK STARTS
   * *******************
   */
  app.get(["/_livez", "/_healthz", "/_readyz"], (req, res) => {
    return res.json({ ok: "ok" });
  });

  app.use(["/hi", "/hello"], (req, res) => {
    return res.json({
      version: packageJson.version,
      title: packageJson.name,
      description: packageJson.description,
      message: "Welcome to Chola",
    });
  });

  /**
   * *****************
   * HEALTH BLOCK ENDS
   * *****************
   */
  app.use("/", fdkExtension.fdkHandler);

  const apiRoutes = fdkExtension.apiRoutes;

  apiRoutes.use("/v1.0/platform", requestLogger, fdkProxyRouter);

  apiRoutes.use("/test", (req, res) => {
    res.send("hello API routes here");
  });

  app.use("/api", apiRoutes);

  apiRoutes.all("*", function () {
    throw new Error("not found");
  });

  //if no route found matched , will return this
  app.get("*", (req, res) => {
    res.contentType("text/html");
    res.sendFile(path.join(__dirname, "./../../../", "build/index.html"));
  });
  return Promise.resolve(app);
}

module.exports = {
  init,
};
